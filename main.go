package main

import (
	"fmt"
	"regexp"
)

func checkCharacter(input string) {

	var isStringAlphabetic = regexp.MustCompile(`^[a-zA-Z]+$`).MatchString
	var isStringAlphabeticUpper = regexp.MustCompile(`^[A-Z]+$`).MatchString
	var isStringAlphabeticLower = regexp.MustCompile(`^[a-z]+$`).MatchString

	if !isStringAlphabetic(input) {
		fmt.Println("Invalid Input")
	} else {
		if isStringAlphabeticUpper(input) {
			fmt.Println("All Capital Letter")
		} else if isStringAlphabeticLower(input) {
			fmt.Println("All Small Letter")
		} else {
			fmt.Println("Mix")
		}
	}

}

func main() {
	var input string
	fmt.Printf("Enter your input : ")
	fmt.Scanln(&input)
	for len(input) > 10000 {
		fmt.Printf("Input must not more than 10000, Please Enter new input : ")
		fmt.Scanln(&input)
	}
	checkCharacter(input)
}
